Jij bent een kraanbestuurder in de haven van Rotterdam. Je werk is erg saai en niemand weet waarom (managers, right?) maar het is erg belangrijk dat je zorgvuldig een lijst met instructies volgt om gestapelde en gemarkeerde containers te verplaatsen. Dit is om ervoor te zorgen dat geen van de containers worden geplet of omvallen. Nadat de containers zijn herschikt, zullen de gewenste containers bovenop elke stapel liggen en kunnen we beginnen met het lossen van het schip.

Als voorbeeld van hoe de kraaninstructies werken, overweeg de volgende invoer:

```
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
```
In dit voorbeeld zijn er drie stapels containers. Stapel 1 bevat twee containers: container `Z` staat onderop en container `N` staat bovenop. Stapel 2 bevat drie containers; van onder naar boven zijn dat containers `M`, `C`, en `D`. Ten slotte bevat stapel 3 een enkele container, `P`.

Vervolgens wordt de herschikkingsprocedure gegeven. In elke stap van de procedure wordt een hoeveelheid containers van de ene stapel naar een andere stapel verplaatst. In de eerste stap van de bovenstaande herschikkingsprocedure wordt één container van stapel 2 naar stapel 1 verplaatst, wat resulteert in deze configuratie:

```
[D]        
[N] [C]    
[Z] [M] [P]
 1   2   3
```
In de tweede stap worden drie containers van stapel 1 naar stapel 3 verplaatst. Containers worden **één voor één** verplaatst, dus de eerste container die wordt verplaatst (`D`) eindigt onder de tweede en derde containers:

```
        [Z]
        [N]
    [C] [D]
    [M] [P]
 1   2   3
```
Vervolgens worden beide containers van stapel 2 naar stapel 1 verplaatst. Opnieuw, omdat containers **één voor één** worden verplaatst, eindigt container `C` onder container `M`:

```
        [Z]
        [N]
[M]     [D]
[C]     [P]
 1   2   3
```
Ten slotte wordt één container van stapel 1 naar stapel 2 verplaatst:

```
        [Z]
        [N]
        [D]
[C] [M] [P]
1   2   3
```
Uiteindelijk, om te controleren of je alles goed heb gedaan, wilt je baas alleen weten **welke container bovenop elke stapel terechtkomt**; in dit voorbeeld zijn de bovenste containers `C` in stapel 1, `M` in stapel 2, en `Z` in stapel 3, dus je moet deze combineren en je baas het bericht `CMZ` geven.

**Na voltooiing van de herschikkingsprocedure, welke container komt bovenop elke stapel terecht?**

Met credits naar de Advent of Code 2022 [dag 5](https://adventofcode.com/2022/day/5)
