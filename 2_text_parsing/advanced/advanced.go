package advanced

// Function for error handling
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Run is the entry point for this package
func Run() string {
	// TODO: Read input from file, process it and fill the result

	// Return the result
	return "TODO: Implement me!"
}
