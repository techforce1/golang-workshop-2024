package main

import (
	"fmt"
	"parsing/advanced"
	"parsing/basic"
)

func main() {
	fmt.Printf("Answer basic   : %d\n", basic.Run())
	fmt.Printf("Answer advanced: %s\n", advanced.Run())
}
