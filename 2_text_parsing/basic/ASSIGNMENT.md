Jij bent een deelnemer in een wereldkampioenschap van Steen Papier Schaar!
Steen Papier Schaar is een spel tussen twee spelers. Elk spel bevat vele rondes; in elke ronde kiezen de spelers elk simultaan een van Steen, Papier, of Schaar met behulp van een handvorm. Vervolgens wordt een winnaar voor die ronde geselecteerd: Steen verslaat Schaar, Schaar verslaat Papier, en Papier verslaat Steen. Als beide spelers dezelfde vorm kiezen, eindigt de ronde in een gelijkspel.

Gelukkig, door het gebruik van geavanceerde AI-gedachte uit een van onze vorige workshops, heb je een model gemaakt dat nauwkeurig kan voorspellen wat je tegenstander gaat spelen! De input (`input.txt`) is een lijst van voorspellingen van je aankomende wedstrijden. De eerste kolom vertegenwoordigt wat je tegenstander gaat spelen: `A` voor Steen, `B` voor Papier, en `C` voor Schaar. Elke keer winnen zou verdacht zijn, dus daarom staat in de tweede kolom wat je het beste kunt spelen als reactie: `X` voor Steen, `Y` voor Papier, en `Z` voor Schaar.

De winnaar van het hele toernooi is de speler met de hoogste score. Je **totaalscore** is de som van je scores voor elke ronde. De score voor een enkele ronde is de score voor de **vorm die je hebt gekozen** (1 voor Steen, 2 voor Papier, en 3 voor Schaar) plus de score voor de **uitkomst van de ronde** (0 als je verliest, 3 als de ronde eindigt in een gelijkspel, en 6 als je wint).

Om er zeker van te zijn dat je de output van de AI begrijpt, kreeg je een strategiegids.

Bijvoorbeeld, stel dat je de volgende strategiegids hebt gekregen:

```
A Y
B X
C Z
```

Deze strategiegids voorspelt en beveelt het volgende aan:

- In de eerste ronde kiest je tegenstander Steen (`A`), en moet jij Papier (`Y`) kiezen. Dit eindigt in een winst voor jou met een score van **8** (2 omdat je Papier koos + 6 omdat je won).
- In de tweede ronde kiest je tegenstander Papier (`B`), en moet jij Steen (`X`) kiezen. Dit eindigt in een verlies voor jou met een score van **1** (1 + 0).
- De derde ronde eindigt in een gelijkspel met beide spelers die Schaar kiezen, wat je een score van 3 + 3 = **6** oplevert.

In dit voorbeeld zou je, als je de strategiegids zou volgen, een totale score van **15** krijgen (8 + 1 + 6).

**Wat zou je totale score zijn als alles precies volgens je strategiegids verloopt?**

Met credits naar de Advent of Code 2022 [dag 2](https://adventofcode.com/2022/day/2)
