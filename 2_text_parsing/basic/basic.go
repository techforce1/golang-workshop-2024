package basic

import "fmt"

// Match is a struct to represent a match in the data.
type Match struct{}

// Function for error handling
func check(e error) {
	if e != nil {
		panic(e)
	}
}

// Function to read input from file and return a slice of Matches
func read_input(filename string) []Match {
	// Initialize slice of Matches
	matches := make([]Match, 0)

	// TODO: Read input from file and append to matches

	// Return the full slice of Matches
	return matches
}

// Run is the entry point for this package
func Run() int {
	// Read input from file
	matches := read_input("basic/input.txt")
	fmt.Println(matches)

	total := 0

	// TODO: Calculate the total score of each match and add to total.

	// Return the total score
	return total
}
