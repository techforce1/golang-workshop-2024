Dit is de tweede opgave in de GoLang workshop. Hier zullen we een aantal van de concepten die we in de vorige opgave hebben gezien herhalen, zoals loops, if-statements, switches, slices, io en error handling.
Andere items die we hier zullen behandelen:

1. Functions
2. Structs
3. Methods

De opgave bestaat uit twee delen: de `basic` en `advanced` opgave.
Elk deel bevat een `ASSIGNMENT.md`, `input.txt` en go-bestand. Lees eerst de opdracht in de `ASSIGNMENT.md` voordat je aan de opgave begint.
Het wordt ook aanbevolen om eerst met de `basic` opgave te beginnen voordat je met de `advanced` opgave begint.

## Functies

```go
func add(a int, b int) int {
    return a + b
}
```

## Structs

```go
type Person struct {
    Name string
    Age  int
}
```

## Methods

```go
type Animal struct {
    Name string
}

func (a *Animal) Speak() {
    fmt.Println("...")
}

type Dog struct {
    Animal
}

func (d *Dog) Speak() {
    fmt.Println("Woof!")
}
```
