Dit is de derde opgave in de GoLang workshop.
We zullen drie concepten uitvoerig gebruiken:
1. [Channels](https://gobyexample.com/channels)
2. [Goroutines](https://gobyexample.com/goroutines)
3. [WaitGroups](https://gobyexample.com/waitgroups)(advanced)

De opgave heeft twee delen, basic en advanced. Ik zou basic snel afmaken en dan aan advanced beginnen.

---

## Channels

```go
channel := make(chan int) // blocking after 1 write
channel <- 1
channel <- 2 // blocks here

bufferedChannel := make(chan int, 2) // blocking after 2 writes
bufferedChannel <- 1
bufferedChannel <- 2
bufferedChannel <- 3 // blocks here (forever)

otherBufferedChannel := make(chan int, 2)
otherBufferedChannel <- 3
otherBufferedChannel <- 5
close(otherBufferedChannel)
resA := <- otherBufferedChannel // resA would get 3
resB := <- otherBufferedChannel // resB would get 5
resC, ok := <- otherBufferedChannel // resC would get 0, ok would get false, because closed

for i := range channel {
    fmt.Println(i)
}

func receiveFromChannel(ch chan<- int)

func sendToChannel(ch <-chan int)
```

## Goroutines

```go
go heavyComputingFunction(channel1)
go heavyComputingFunction(channel2)
go heavyComputingFunction(channel3)

go func() {
    // computing stuff
}()
```

## Waitgroup

```go
wg := sync.WaitGroup{}

wg.Add(3)

for i:=0; i < 3; i++ {
    go func() {
        defer wg.Done()
        heavyComputingFunction()
    }()
}
wg.Wait() // wait for all 3 heavy computation to complete

// ---
wg := sync.WaitGroup{}
wg.Add(3)
for i:=0; i < 2; i++ {
    go func() {
        defer wg.Done()
        heavyComputingFunction()
    }()
}
wg.Wait() // Never completes because only 2 will ever be Done.
```