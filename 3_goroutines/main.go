package main

import (
	"fmt"
	"goroutine/advanced"
	"goroutine/basic"
)

func main() {
	fmt.Printf("Answer basic   : %t\n", basic.Run())
	fmt.Printf("Answer advanced: %s\n", advanced.Run())
}
