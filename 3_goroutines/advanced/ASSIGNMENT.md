Voor de wat geavanceerdere opdracht gaan we de zeef van Eratosthenes implementeren in parallel.

Eerst wat leuke feitjes over priemgetallen
1. Een priem getal is een getal waarbij de enige delers 1 en zichzelf zijn en een geheel getal oplevert. Zo is 5 deelbaar door 1 en 5, maar niet door 2 want dat geeft 2,5.
2. Even getallen groter dan 2 zijn nooit een priemgetal
3. Elk getal dat geen priemgetal is, is een product van priemgetallen. Zo is `12 = 2 * 2 * 3` .
4. Als je wilt zien of een getal een priemgetal is hoef je alleen alle priem getallen te bekijken tot de wortel van dat getal. De wortel van 603729 is 777, dus hoef je alleen alle priem getallen tot en met 777 te checken.
5. Dat zorgt er ook voor dat als je alle priem getallen tot 777 weet, je daarmee makkelijk de priem getallen tot 603729 berekent.

# Zeef van Eratosthenes

## Hoe werkt de zeef
Neem een lijst van getallen, `2` tot `x`.
Neem het eerste getal van deze lijst en haal de veelvouden (niet het getal zelf) uit de lijst. Neem dan het volgend getal (`3`) , en verwijder de veelvouden. Ga zo door tot je alleen nog priem getallen overhoud.
Zodra we een getal uit de lijst nemen die groter is dan de wortel van `x` weten we dat we alle priemgetallen in de lijst gevonden hebben.

## Hoe werkt de zeef in parallel?
Het wegstrepen van de getallen is het echte werk, dit kunnen we makkelijk in parallel doen door delen van de lijst op verschillende cores weg te strepen.

# De Oefening

## Maak een channel die meervouden van een getal binnen een range teruggeeft

Implementeer `MultiplesOfNumberInRange(start, end int, number int, ch chan<- int)`. 
Deze functie stopt meervouden van `number` in de `ch` channel.
Deze nummers lopen vanaf `start` tot `end`, `end` is exclusive.

`firstNumberInRange := start - (start % number) + number`

Omdat we alleen geïntereseerd zijn de meervouden van `number`, leveren we `number` niet mee.

Voorbeeld `MultiplesOfNumberInRange(12, 20, 2, ch chan<- int)` geeft: `[12,14,16,18]`.

## Kijk of de testen voor de multiples werken.
Draai de testen in de `advanced_test.go` file.

## Verwijder multiples uit een lijst

Implementeer `RemoveMultiples(offset, number int, candidates []bool)`.
Candidates is een lijst met mogelijke priem getallen.
Hier duid de positie in de lijst welk getal het is + offset.

Als de offset gelijk is aan `3`, dan geeft `candidates[0]` aan of het getal `3` al weggestreept is. Op dezelfde manier gaat `candidates[1]` over getal 4, en `candidates[901]` over getal `3+901=904`.

Dus als `candidate[i]` gelijk is aan `true` dan is deze weggestreept en is getal `i+offset` *geen* priemgetal.

Deze functie muteert de `candidates` lijst en streept meervouden van `number` weg door ze op `true` te zetten.

`candidates` list is passed by reference.

Gebruik de `MultiplesOfNumberInRange` functie die je net geïmplementeerd hebt als een goroutine.

## Kijk of de testen voor RemoveMultiples werken!

## De eerste zeef

Implementeer `SievePrimes(start, end int, existingPrimes []int, primeChannel chan<- int)`.
Deze functie doet een paar dingen:

1. Maakt een lijst van candidaten aan voor de nummers van `start` tot `end` (`end` exclusive).
2. Gebruikt `RemoveMultiples` om meervouden van `existingPrimes` weg te strepen.
3. Zet elke niet weggestreepte candidate op de `primeChannel` want dat is een priem getal
4. sluit de `primeChannel`

Nu blijkt dat we het wegstrepen wel in parallel kunnen doen.
We gaan golang's `sync.WaitGroup{}` gebruiken.
```go
wg := sync.WaitGroup{}
wg.Add(5)

for i:=0; i < 5; i++ {
    nonLoopCaptured := i
    go func() {
        defer wg.Done()
        // Extremely heavy task
        fmt.Println("done with task", nonLoopCaptured)
    }
}
wg.Wait()
```

Een WaitGroup is een teller waar met `Add(x)` kunnen aangeven dat er een `x` aantal taken zullen gebeuren.
Deze taken vinden plaats in een goroutine.
Als een taak klaar is geeft deze dat aan met `Done()`.

Buiten de goroutine wachten we met `Wait()` tot alle taken Done zijn voor we verder gaan.

Met deze wetenschap kunnen we het wegstrepen van meervouden in parallel doen.

## Het echte berekenen van priemgetallen!

We gaan de `CalculatePrimes(oldPrimes []int) []int` functie implementeren.
Op basis van het laatste priem getal in `oldPrimes` kunnen we berekenen welke range van priemgetallen we allemaal kunnen berekenen.
Met `lp := oldPrimes[len(oldPrimes) - 1]`, kunnen we de priemgetallen van `lp` tot `lp*lp` berekenen.

Deze range kan verdeeld worden over parallele executies.
Laten we de range in dit geval over 4 verspreiden.

Vergeet niet 4 channels te maken met *buffers*, zodat die niet meteen blokkeren als er iets in gestopt wordt.

Voeg de gegenereerde priemgetallen in de goede volgorde samen.

## Run!

Gebruik de `CalculatePrimes` methode 3 keer in de `Run()` functie, en print het resultaat.

Bekijk of alle cores gebruikt worden.