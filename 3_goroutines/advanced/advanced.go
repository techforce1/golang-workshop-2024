package advanced

import (
	"fmt"
	"runtime"
)

// func MultiplesOfNumberInRange(start, end int, number int, ch chan<- int)

// func RemoveMultiples(offset, number int, candidates []bool)

// func SievePrimes(start, end int, existingPrimes []int, primeChannel chan<- int) {
// 	defer close(primeChannel)

// 	candidates := make([]bool, end-start)

// 	for _, prime := range existingPrimes {
// 		RemoveMultiples(start, prime, candidates)
// 	}

// 	for index, c := range candidates {
// 		if !c {
// 			primeChannel <- index + start
// 		}
// 	}

// }

// func CalculatePrimes(oldPrimes []int) []int

func Run() string {
	runtime.GOMAXPROCS(runtime.NumCPU())
	primes := []int{2, 3, 5, 7, 11}

	// primes = CalculatePrimes(primes)
	fmt.Println("cycle1", len(primes))
	// primes = CalculatePrimes(primes)
	fmt.Println("cycle2", len(primes), primes[len(primes)-1])
	// primes = CalculatePrimes(primes)
	fmt.Println("Final", len(primes), primes[len(primes)-1])

	return "Advanced done!"
}
