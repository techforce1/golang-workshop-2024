package advanced

import (
	"testing"
)

func TestMultipleOfNumbers2(t *testing.T) {
	ch := make(chan int)
	go MultiplesOfNumberInRange(12, 20, 2, ch)

	values := []int{12, 14, 16, 18}

	for _, val := range values {
		a := <-ch
		if a != val {
			t.Errorf("Invalid multiple, expected %d was %d", val, a)
		}
	}
	_, ok := <-ch
	if ok {
		t.Error("not closed")
	}
}

func TestMultipleOfNumbers3(t *testing.T) {
	ch := make(chan int)
	go MultiplesOfNumberInRange(11, 15, 3, ch)

	values := []int{12}

	for _, val := range values {
		a := <-ch
		if a != val {
			t.Errorf("Invalid multiple, expected %d was %d", val, a)
		}
	}
	_, ok := <-ch
	if ok {
		t.Error("not closed")
	}
}

func TestRemoveMultipleOf3(t *testing.T) {
	// [3, 4, 5, 6, 7, 8, 9, 10]
	candidates := []bool{false, false, false, false, false, false, false, false}

	RemoveMultiples(3, 3, candidates)

	// candidates should be changed to
	// [3, 4, 5, (6), 7, 8, (9), 10]
	for i, v := range []bool{false, false, false, true, false, false, true, false} {
		if v != candidates[i] {
			t.Errorf("Expected %t, was %t at [%d]", v, candidates[i], i+3)
		}
	}
}

func TestPrimenumbers(t *testing.T) {
	primes := []int{2, 3, 5}

	primes = CalculatePrimes(primes)
	primes = CalculatePrimes(primes)

	manyPrimes := []int{
		2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71,
		73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173,
		179, 181, 191, 193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263, 269, 271, 277, 281,
		283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409,
		419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523,
	}

	if len(primes) != len(manyPrimes) {
		t.Errorf("Expected %d primes, but got %d", len(manyPrimes), len(primes))
	}

	for i, v := range manyPrimes {
		if primes[i] != v {
			t.Errorf("Expected %d, got %d", v, primes[i])
		}
	}
}
