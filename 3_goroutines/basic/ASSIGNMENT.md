Voor de basic opdracht gebruiken we de [tree go package]("golang.org/x/tour/tree").

Dit levert ons een simpele `binary tree` structuur en een manier om binary trees te maken.

Bekijk het plaatje:

![De binary tree](tree.png)

De eigenschap van een binary tree is dat het uit knopen is opgebouwd. Een knop kan twee kinderen hebben, een linker kind en een rechter kind. De linker kind heeft een waarde gelijk of lager de knoop, en een rechter kind heeft een waarde hoger dan de knoop. In het plaatje kan je zien dat de knoop met waarde 3, twee kinderen heeft. Het linker kind heeft een lagere waarde, en het rechter kind een hogere.

De linker boom en de rechter boom zijn twee verschillende bomen, maar hebben dezelfde waardes.

De boom heeft de volgende structuur:

```go
type Tree struct {
    Left  *Tree
    Value int
    Right *Tree
}
```

## Implementeer de Walk functie.

Loop alle waardes van klein naar groot langs en stuur ze naar de channel.

## Implementeer de Same functie.

Bekijk of twee verschillende bomen dezelfde waardes hebben.

## Schrijf een test in `basic_test.go` file

