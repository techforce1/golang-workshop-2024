Als oefening om de basis Go dingen onder de knie te krijgen gaan we een techforce1 hashfunctie schrijven.
Wanneer er tijd over is, dan gebruiken we deze hashfunctie gebruiken om een hash map te schrijven.

## Het inlezen van een lijst met namen om te hashen.

Als eerste gaan we een lijst inlezen. In het `main.go` bestand staat een `ReadLines` functie die we in gaan vullen.

Gebruik `os.ReadFile` om een file in te lezen, in dit gegval `names.txt`. 
Bekijk de beschrijving van `os.ReadFile` op de website: https://pkg.go.dev/os#ReadFile.

- Gebruik google om uit te vogelen hoe je een array van bytes naar een string kunt omzetten.
- Vergeet niet de error af te handelen.
- Gebruik [`strings.Split`](https://pkg.go.dev/strings#Split) om te splitsen op alle regels. Splits op `\n` of `\r\n`, bekijk welke werkt.

## Berekenen van een priem getal

Priemgetallen zijn een van mijn favoriete dingen, ze zijn super cool en worden voor allerlei dingen gebruikt.
Zo ook in de Techforce1 hash.

In het `main.go` bestaand staat een functie genaamd `isPrime`.

Implementeer de functie om te kijken of de parameter `number` een priemgetal is.

- Een priemgetal kan niet gedeeld worden door een getal hoger dan 1 en lager dan het priemgetal zelf.
- Is een getal deelbaar, dan is het restgetal gelijk aan 0 na een modulo. Dus: `getal % deler == 0` (getal modulo deler is gelijk aan 0)

## Berekenen van de hash

Voor deze hash gebruiken we de eerste 200 priem getallen die bestaan. Hier kunnen we de `calculatePrimes(amount)` functie voor gebruiken.

Dan converteren we de parameter string naar een byte array: `[]byte(name)`, en declareren we een hash variabele met waarde 0.

We gaan deze hash variabele in 256 rondes steeds verder aanpassen, dus in een for loop.

Voor elke byte in de byte array voegen we een additie toe aan de hash: de byte vermenigvuldigt met het priem getal op plek 'index van de loop' modulo (%) 200.

Nu declareren we drie variabeles: a, b en c.
a is de huidige hash gedeeld door 5, b is de huidige hash vermenigvuldigt met 1223, en c is de huidige hash modulo(%) 1129.

Op basis van de hash gaan we 7 verschillende acties uitvoeren op basis van `hash modulo(%) 7`.

- Als het restgetal 0 is, dan zetten we de hash op `a+b-c`.
- Als het restgetal 1 is, dan zetten we de hash op `hash keer 2`
- Als het restgetal 2 is, dan zetten we de hash op `hash gedeeld 2`
- Als het restgetal 3 is, dan zetten we de hash op `a+c`
- Als het restgetal 4 is, dan zetten we de hash op `a-(b*2)`
- Als het restgetal 5 is, dan zetten we de hash op `a+b`
- Als het restgetal 6 is, dan zetten we de hash op `(b and c) or a`, `and = &`, `or = |`

Als laatste instructie van de loop zetten we de hash op hash keer het priem getal op plek 'index van de loop' modulo(%) 200.

Als de loop helemaal klaar is, gebruiken we als hash resultaat de hash modulo `18446744073709551557`.

## Gebruik van de hash

Nu dat we een hash gemaakt hebben moeten we het ook even uitproberen.
In de functie: `main()`. Lees een lijst met namen door de `ReadLines()` functie te gebruiken.
Print voor elke naam de hash.

## TechforceMap

Als je nog tijd over hebt gaan we een simpele hashmap maken.
Deze hashmap, `TechforceMap`, bevat een `values` array waar 256 waardes in gestopt kunnen worden. Deze waardes zijn [LinkedLists](https://en.wikipedia.org/wiki/Linked_list).

### Put

Implementeer de Put functie.

1. Hash de key met de Techforce1 Hash.
2. Gebruik deze waarde om iets in de `values` array te stoppen van de map.
3. Als er **niets** op deze positie staat, dan komt er een nieuw `LinkedListNode`, anders breiden we de bestaande lijst uit met een nieuwe `LinkedListNode`.

### Get

Implementeer de Get functie

1. Je kunt het!
2. Vertrouw op jezelf!

### Test

Schrijf een test om de map te testen.
Een file die eindigt op `_test.go` is een test file.