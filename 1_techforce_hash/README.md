Dit is de eerste opgave in de GoLang workshop.
We zullen een paar dingen uit go geburiken:

1. for loops
2. switch
3. if 
4. slices

De opgave bestaat uit twee delen: Een hash functie maken, en daarna deze gebruiken voor een simpele hashmap.

---


## Operations

```go

sum := 2 + 2 //
minus := 2 - 1 = 0

divide := 5 / 2 // 5 / 2 = 2
modulo := 5 % 2 // 5 % 2 = 1

list := []int{1,2,3,4,5}
index := list[2] // val = 3

casting := uint64(index)

bitwiseAnd := 0b1100 & 0b0101 // = 0b0100
bitwiseOr := 0b1100 | 0b0101 // = 0b1101

```

## If statements

```go
if 1 == 2 {
    fmt.Println("1 equals 2 and this is proof")
} else {
    fmt.Println("...")
}
```

## switch statement

```go
switch number {
    case 0:
        fmt.Println("0")
    case 1:
        fmt.Println("1")
    case 2:
        fmt.Println("2")
    case 3:
        fmt.Println("3")
    default:
        fmt.Println("other:", number)        
}
```

## For loops

```go
for i := 3; i < 41; i+=2 {
    fmt.Println(i)
}

list := []int{2,3,5,7,11,13,17}
for i := range list {
    fmt.Println(i)
}

for i := 0;; i++ {
    fmt.Println(i)
}

for {
    fmt.Println("infinite loops")
}
```