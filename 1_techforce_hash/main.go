package main

// Make sure to have correct line endings in the file (/n instead of /r/n)
func ReadLines() ([]string, error)

func isPrime(number int, primes []int) bool

func TechforceHash(name string)

func main() {

}

// func (m *TechforceMap) put(key string, value uint64)

// func (m *TechforceMap) get(key string) (uint64, error)

type TechforceMap struct {
	values []LinkedListNode
}

type LinkedListNode struct {
	key   string
	value uint64
	next  *LinkedListNode
}

func newTechforceMap() *TechforceMap {
	return &TechforceMap{
		values: make([]LinkedListNode, 256),
	}
}

func calculatePrimes(amount int) []int {
	if amount == 0 {
		return []int{}
	}

	primes := make([]int, amount)
	primes[0] = 2
	counter := 3

	for index := 1; index < amount; {
		if isPrime(counter, primes) {
			primes[index] = counter
			index++
		}
		counter += 2
	}
	return primes
}
